package com.vinayrraj.flipdigit.lib;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * @author VinayrajSingh
 * 
 */
public class Flipmeter extends LinearLayout {
	private static int NUM_DIGITS = 0;

	private int mCurrentValue;
	private int animationCompleteCounter = 0;

	private FlipmeterSpinner[] mDigitSpinners;

	public Flipmeter(Context context) {
		super(context);

	}

	public Flipmeter(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public void setDigits(int digit) {
		NUM_DIGITS = digit;
		mDigitSpinners = new FlipmeterSpinner[NUM_DIGITS];
	}

	private void initialize() {

		// Inflate the view from the layout resource.
		String infService = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li;
		li = (LayoutInflater) getContext().getSystemService(infService);
		li.inflate(R.layout.widget_flipmeter, this, true);

		setSpinners(NUM_DIGITS);

	}

	public void setValue(int value, boolean withAnimation) {

		initialize();

		mCurrentValue = value;
		int tempValue = value;

		for (int i = NUM_DIGITS - 1; i > 0; --i) {
			int factor = (int) Math.pow(10, i);

			int digitVal = (int) Math.floor(tempValue / factor);
			tempValue -= (digitVal * factor);
			mDigitSpinners[i].setDigit(digitVal, withAnimation);
			changeAnimationCompleteCounter(withAnimation);
		}

		mDigitSpinners[0].setDigit(tempValue, withAnimation);
		changeAnimationCompleteCounter(withAnimation);

	}

	public void setValueAfterInitize(int value, boolean withAnimation) {

		setSpinners(NUM_DIGITS);

		mCurrentValue = value;
		int tempValue = value;

		for (int i = NUM_DIGITS - 1; i > 0; --i) {
			int factor = (int) Math.pow(10, i);

			int digitVal = (int) Math.floor(tempValue / factor);
			tempValue -= (digitVal * factor);
			mDigitSpinners[i].setDigit(digitVal, withAnimation);
			changeAnimationCompleteCounter(withAnimation);
		}

		mDigitSpinners[0].setDigit(tempValue, withAnimation);
		changeAnimationCompleteCounter(withAnimation);

	}

	private void setSpinners(int NUM) {

		if (NUM == 1) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
		} else if (NUM == 2) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
			mDigitSpinners[1] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
		} else if (NUM == 3) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
			mDigitSpinners[1] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
			mDigitSpinners[2] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100);
		} else if (NUM == 4) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
			mDigitSpinners[1] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
			mDigitSpinners[2] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100);
			mDigitSpinners[3] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1k);
		} else if (NUM == 5) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
			mDigitSpinners[1] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
			mDigitSpinners[2] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100);
			mDigitSpinners[3] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1k);
			mDigitSpinners[4] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10k);
		} else if (NUM == 6) {
			mDigitSpinners[0] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1);
			mDigitSpinners[1] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
			mDigitSpinners[2] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100);
			mDigitSpinners[3] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1k);
			mDigitSpinners[4] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10k);
			mDigitSpinners[5] = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100k);
		}

		setViewVisibility(NUM);
	}

	private void setViewVisibility(int NUM) {

		FlipmeterSpinner ten = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10);
		FlipmeterSpinner hundred = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100);
		FlipmeterSpinner thousand = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_1k);
		FlipmeterSpinner ten_thousand = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_10k);
		FlipmeterSpinner hudred_thousand = (FlipmeterSpinner) findViewById(R.id.widget_flipmeter_spinner_100k);
		ImageView comma = (ImageView) findViewById(R.id.comma);

		if (NUM == 1) {
			ten.setVisibility(View.GONE);
			hundred.setVisibility(View.GONE);
			thousand.setVisibility(View.GONE);
			ten_thousand.setVisibility(View.GONE);
			hudred_thousand.setVisibility(View.GONE);
			comma.setVisibility(View.GONE);
		} else if (NUM == 2) {
			ten.setVisibility(View.VISIBLE);
			hundred.setVisibility(View.GONE);
			thousand.setVisibility(View.GONE);
			ten_thousand.setVisibility(View.GONE);
			hudred_thousand.setVisibility(View.GONE);
			comma.setVisibility(View.GONE);
		} else if (NUM == 3) {
			ten.setVisibility(View.VISIBLE);
			hundred.setVisibility(View.VISIBLE);
			thousand.setVisibility(View.GONE);
			ten_thousand.setVisibility(View.GONE);
			hudred_thousand.setVisibility(View.GONE);
			comma.setVisibility(View.GONE);
		} else if (NUM == 4) {
			ten.setVisibility(View.VISIBLE);
			hundred.setVisibility(View.VISIBLE);
			thousand.setVisibility(View.VISIBLE);
			ten_thousand.setVisibility(View.GONE);
			hudred_thousand.setVisibility(View.GONE);
			comma.setVisibility(View.VISIBLE);
		} else if (NUM == 5) {
			ten.setVisibility(View.VISIBLE);
			hundred.setVisibility(View.VISIBLE);
			thousand.setVisibility(View.VISIBLE);
			ten_thousand.setVisibility(View.VISIBLE);
			hudred_thousand.setVisibility(View.GONE);
			comma.setVisibility(View.VISIBLE);
		} else if (NUM == 6) {
			ten.setVisibility(View.VISIBLE);
			hundred.setVisibility(View.VISIBLE);
			thousand.setVisibility(View.VISIBLE);
			ten_thousand.setVisibility(View.VISIBLE);
			hudred_thousand.setVisibility(View.VISIBLE);
			comma.setVisibility(View.VISIBLE);
		}
	}

	private synchronized int changeAnimationCompleteCounter(Boolean increment) {
		if (increment == null)
			return animationCompleteCounter;
		else if (increment)
			return ++animationCompleteCounter;
		else
			return --animationCompleteCounter;
	}

	/**
	 * @return
	 */
	public int getValue() {
		return mCurrentValue;
	}

	public interface OnValueChangeListener {
		abstract void onValueChange(Flipmeter sender, int newValue);
	}

}
